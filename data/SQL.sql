﻿DROP DATABASE IF EXISTS ejemplo03Aplicacion;
CREATE DATABASE ejemplo03Aplicacion;
USE ejemplo03Aplicacion;

CREATE TABLE actividades(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  descripcion varchar(500),
  duracion int,
  imagen varchar(200),
  PRIMARY KEY(id)
);

CREATE TABLE salas(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  descripcion varchar(500),
  plazas int,
  imagen varchar(200),
  PRIMARY KEY(id)
);

CREATE TABLE monitor(
  id int AUTO_INCREMENT,
  nombre varchar(200),
  imagen varchar(200),
  telefono varchar(20),
  correo varchar(100),
  PRIMARY KEY(id)
);

CREATE TABLE realizan(
  id int AUTO_INCREMENT,
  actividad int,
  sala int,
  monitor int,
  fechaHora datetime,
  UNIQUE (actividad,sala,monitor,fechaHora),
  PRIMARY KEY(id)
);

ALTER TABLE realizan
  ADD CONSTRAINT fkRealizanMonitor FOREIGN KEY (monitor) REFERENCES monitor(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fkRealizanSalas FOREIGN KEY (sala) REFERENCES salas(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fkRealizanActividades FOREIGN KEY (actividad) REFERENCES actividades(id)
    ON DELETE CASCADE ON UPDATE CASCADE;


-- vista para el trajador que mas actividades imparte
CREATE VIEW trabajador AS 
SELECT COUNT(*) numero, r.monitor FROM realizan r GROUP BY r.monitor 
  ORDER BY numero DESC LIMIT 1;

-- vista para el trajador que mas actividades imparte
DROP VIEW IF EXISTS trabajador;
CREATE VIEW trabajador AS 
SELECT * FROM 
(SELECT COUNT(*) numero, r.monitor FROM realizan r GROUP BY r.monitor) c2
WHERE numero = 
(SELECT MAX(numero) maximo FROM 
(SELECT COUNT(*) numero, r.monitor FROM realizan r GROUP BY r.monitor) c1); 