<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header id="header">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => ['class' => 'navbar-expand-md navbar-dark bg-dark fixed-top']
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Actividades hoy', 'url' => ['/site/hoy']],
            ['label' => 'Gestion de salas', 'items' => [
                ['label' => 'Administracion de salas', 'url' => ['/salas/index']],
                ['label' => 'Nueva sala', 'url' => ['/salas/create']],
            ]],
            ['label' => 'Gestion de Actividades', 'items' => [
                ['label' => 'Administracion de actividades', 'url' => ['/actividades/index']],
                ['label' => 'Nueva actividades', 'url' => ['/actividades/create']],
            ]],
            ['label' => 'Gestion de Monitores', 'items' => [
                ['label' => 'Administracion de monitores', 'url' => ['/monitor/index']],
                ['label' => 'Nuevo monitor', 'url' => ['/monitor/create']],
            ]],
            ['label' => 'Asociaciones', 'items' => [
                ['label' => 'Administracion de realizan', 'url' => ['/realizan/index']],
                ['label' => 'Nueva realizan', 'url' => ['/realizan/create']],
            ]],
        ]
    ]);
    NavBar::end();
    ?>
</header>

<main id="main" class="flex-shrink-0" role="main">
    <div class="container">
        <?php if (!empty($this->params['breadcrumbs'])): ?>
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        <?php endif ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer id="footer" class="mt-auto py-3 bg-light">
    <div class="container">
        <div class="row text-muted">
            <div class="col-md-6 text-center text-md-start">Gimnasio Alpe Formacion</div>
            <div class="col-md-6 text-center text-md-end">Pasaje de peña 1, Santander</div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
