<?php

/** @var yii\web\View $this */

$this->title = 'Actividades hoy';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gimnasio Alpe Formacion</h1>

        <p class="lead">Actividades de hoy</p>

    </div>
   
</div>

<div class="text-center">
    <?=    \yii\helpers\Html::img("@web/imgs/site/1.png") ?>
</div>

<div>
       <h2>Las actividades disponibles de hoy son</h2>
        <?= yii\grid\GridView::widget([
            "dataProvider" => $dataProvider,
            "columns" => [
                [
                    'attribute' => 'actividad',
                    'value'=> 'actividad0.nombre'
                ],
                [
                    'attribute' => 'duracion',
                    'value'=> 'actividad0.duracion'
                ],
                [
                    'attribute' => 'monitor',
                    'value'=> 'monitor0.nombre'
                ],
                [
                    'attribute' => 'sala',
                    'value'=> 'sala0.nombre'
                ],
                'fechaHora'
            ],
        ]) ?>
</div>



