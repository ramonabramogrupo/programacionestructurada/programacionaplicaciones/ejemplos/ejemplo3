<?php

/** @var yii\web\View $this */

$this->title = 'Actividades para hoy';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gimnasio Alpe Formacion</h1>

        <p class="lead">Actividades que puedo realizar hoy</p>

    </div>
   
</div>

<div class="text-center">
    <?=    \yii\helpers\Html::img("@web/imgs/site/1.png") ?>
</div>

<div>
        <?php 
//        echo yii\grid\GridView::widget([
//            "dataProvider" => $dataProvider
//         ]); 
        ?>
</div>

<div>
        <?= yii\grid\GridView::widget([
            "dataProvider" => $dataProvider1,
            "columns" => [
                [
                    "attribute" => "actividad",
                    "value" => "actividad0.nombre"
                ],
                //"actividad0.nombre",
                "actividad0.duracion",
                "actividad0.descripcion",
                [
                    "attribute" => "imagen",
                    "format" => "raw",
                    "value" => function($modelo){
                        return yii\helpers\Html::img("@web/imgs/actividades/{$modelo->actividad0->imagen}",["style"=>"width:300px"]);
                    }
                ],
                [
                    "attribute" => "sala",
                    "value" => "sala0.nombre",
                ],
                //"sala0.nombre",
                [
                    "attribute" => "monitor",
                    "value" => "monitor0.nombre",
                ],
                //"monitor0.nombre",
                "fechaHora",
                [
                    'label' => 'Mas informacion',
                    'format' => 'raw',
                    'value' => function($modelo){
                        return yii\helpers\Html::a("ver mas...",["realizan/view","id"=>$modelo->id]);
                    }
                ],
                
            ]
         ]) ?>
</div>




