<?php

/** @var yii\web\View $this */

$this->title = 'Gestion de actividades';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gimnasio Alpe Formacion</h1>

        <p class="lead">Gestion de actividades</p>

    </div>
   
</div>

<div class="text-center">
    <?=    \yii\helpers\Html::img("@web/imgs/site/1.png") ?>
</div>

<div>
       <h2>Las actividades disponibles de hoy son</h2>
        <?= yii\grid\GridView::widget([
            "dataProvider" => $dataProvider,
            "columns" => [
               "id",
                "nombre",
                "descripcion",
                "duracion",
                //"imagen",
                [
                'attribute' => 'imagen',
                'format' => 'raw',
                'value' => function($modelo){
                    return yii\helpers\Html::img("@web/imgs/actividades/{$modelo["imagen"]}",['style'=>'width:300px']);
                }
                ],
                [
                  'label' => 'mas informacion', 
                  'format' => 'raw',
                  'value' => function($modelo){
                    return yii\helpers\Html::a('ver mas...',['site/informacion','id'=> $modelo["id"]]);
                  }
                ],
            ],
            
        ]) ?>
</div>

<div>
       <h2>Las actividades mas veces impartidas</h2>
        <?= yii\grid\GridView::widget([
            "dataProvider" => $dataProvider1,
            "columns" => [
                "id",
                "nombre",
                "descripcion",
                "duracion",
                //"imagen",
                [
                'attribute' => 'imagen',
                'format' => 'raw',
                'value' => function($modelo){
                    return yii\helpers\Html::img("@web/imgs/actividades/{$modelo["imagen"]}",['style'=>'width:300px']);
                }
                ],
                "repeticiones",
                 [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{datos} ',
                'buttons'=> [ 
                    'datos' => function($url,$modelo){
                        return "si";
                    }
                    ]
                ],
                        
            ],
           
        ]) ?>
</div>


