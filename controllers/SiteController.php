<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        // $consulta= \Yii::$app->db->createCommand('select * from actividades')->queryAll();
        
        /**
         * mostrar las actividades que se realizan hoy
         */
        $dataProvider=new \yii\data\SqlDataProvider([
            'sql' => 'SELECT 
                        DISTINCT a.* 
                            FROM realizan r JOIN actividades a ON r.actividad = a.id
                            WHERE DATE_FORMAT(fechaHora,"%Y-%m-%d")=CURDATE()'
        ]);
        
        /**
         * mostrar las activades que mas veces se han impartido
         */
        $dataProvider1=new \yii\data\SqlDataProvider([
           'sql' => 'SELECT a.id,
                        a.nombre,
                        a.descripcion,
                        a.duracion,
                        a.imagen,
                        c1.repeticiones
                        FROM actividades a JOIN
                            (SELECT 
                              COUNT(*) repeticiones, r.actividad 
                              FROM realizan r 
                              GROUP BY r.actividad
                              ORDER BY repeticiones DESC) c1 ON a.id= c1.actividad' 
        ]);
        
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProvider1,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionHoy(){
        $dataProvider=new \yii\data\SqlDataProvider([
            "sql" => 'SELECT a.nombre,
                        a.descripcion,
                        a.duracion,
                        a.imagen,
                        c1.sala,
                        c1.monitor,
                        c1.fechaHora  
                        FROM actividades a JOIN 
                        (SELECT *
                            FROM realizan r 
                            WHERE DATE_FORMAT(fechaHora,"%Y-%m-%d")=CURDATE() 
                            AND r.fechaHora>NOW())c1 ON a.id=c1.actividad'
        ]);
        
        // realizar una consulta similar pero con activedataprovider
        $consulta= \app\models\Realizan::find()
                    ->where('DATE_FORMAT(fechaHora,"%Y-%m-%d")=CURDATE() AND fechaHora>NOW()');
        
        $dataProvider1=new \yii\data\ActiveDataProvider([
           "query" => $consulta 
        ]);
        
        return $this->render("hoy",[
            "dataProvider" => $dataProvider,
            "dataProvider1" => $dataProvider1
        ]);
    }
    
    public function actionInformacion($id){
        $consulta= \app\models\Realizan::find()
                    ->where("actividad={$id} AND DATE_FORMAT(fechaHora,'%Y-%m-%d')=CURDATE()");
        
        $dataProvider=new \yii\data\ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("informacion",[
            "dataProvider" => $dataProvider
        ]);
    }
}
